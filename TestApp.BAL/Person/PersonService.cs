﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.BAL.BusinessModels;
using TestApp.DAL.DataModels;
using TestApp.DAL.Repositories;

namespace TestApp.BAL.Person
{
    public interface IPersonService
    {
        IEnumerable<IPersonModel> GetEveryPerson();
        Task<IEnumerable<IPersonModel>> GetEveryPersonAsync();
        Task<IPersonModel> GetPersonAsync(int id);
        Task AddPersonAsync(IPersonModel person);
        Task UpdatePersonAsync(IPersonModel person);
    }

    public class PersonService : IPersonService
    {
        protected readonly IRepo<IPerson> _personRepo;

        public PersonService(IRepo<IPerson> repo)
        {
            _personRepo = repo;
        }

        public async Task AddPersonAsync(IPersonModel personModel)
        {
            await _personRepo.Add(MapPerson(personModel));
        }

        public async Task UpdatePersonAsync(IPersonModel personModel)
        {
            await _personRepo.Update(MapPerson(personModel));
        }

        public IEnumerable<IPersonModel> GetEveryPerson()
        {
            return _personRepo.GetAll()
                .Select(x => new PersonModel
                {
                    Firstname = x.Firstname,
                    Surname = x.Surname,
                    Gender = x.Gender,
                    EmailAddress = x.EmailAddress,
                    PhoneNumber = x.PhoneNumber
                }).ToList();
        }

        public async Task<IEnumerable<IPersonModel>> GetEveryPersonAsync()
        {
            var listOfPeople = new List<IPersonModel>();
            await _personRepo.GetAllAsync()
                .ContinueWith((x) =>
                {
                    listOfPeople.AddRange(x.Result.Select(person => new PersonModel
                    {
                        PersonId = person.PersonId,
                        Firstname = person.Firstname,
                        Surname = person.Surname,
                        Gender = person.Gender,
                        EmailAddress = person.EmailAddress,
                        PhoneNumber = person.PhoneNumber
                    }).Cast<IPersonModel>());
                });
            return listOfPeople;
        }

        public async Task<IPersonModel> GetPersonAsync(int id)
        {
            var person = new PersonModel();
            await _personRepo.Get(id)
                .ContinueWith((x) =>
                {
                    person.Firstname = x.Result.Firstname;
                    person.Surname = x.Result.Surname;
                    person.Gender = x.Result.Gender;
                    person.EmailAddress = x.Result.EmailAddress;
                    person.PhoneNumber = x.Result.PhoneNumber;
                });
            return person;
        }

        #region MyRegion

        private static DAL.DataModels.Person MapPerson(IPersonModel personModel)
        {
            var person = new DAL.DataModels.Person
            {
                Firstname = personModel.Firstname,
                Surname = personModel.Surname,
                Gender = personModel.Gender,
                EmailAddress = personModel.EmailAddress,
                PhoneNumber = personModel.PhoneNumber
            };
            return person;
        }

        #endregion
    }
}
