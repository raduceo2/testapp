﻿using System.ComponentModel.DataAnnotations;

namespace TestApp.BAL.BusinessModels
{
    public interface IPersonModel
    {
        int PersonId { get; set; }
        string Firstname { get; set; }
        string Surname { get; set; }
        string Gender { get; set; }
        string EmailAddress { get; set; }
        string PhoneNumber { get; set; }
    }

    public class PersonModel : IPersonModel
    {
        public int PersonId { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        //email validation
        public string EmailAddress { get; set; }
        //phone number validation
        public string PhoneNumber { get; set; }
    }
}
