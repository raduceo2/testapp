﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TestApp.BAL.BusinessModels;
using TestApp.BAL.Person;
using TestApp.Filters;
using WebApi.OutputCache.V2;

namespace TestApp.Controllers
{
    [RoutePrefix("api/person")]
    public class PersonApiController : ApiController
    {
        private readonly IPersonService _service;

        public PersonApiController(IPersonService service)
        {
            _service = service;
        }

        [Route("all")]
        //[CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100)]
        public async Task<IHttpActionResult> GetAll()
        {
            return Ok(await _service.GetEveryPersonAsync());
        }

        [Route("get/{id:int}")]
        public async Task<IHttpActionResult> GetPerson(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Invalid id");
            }

            return Ok(await _service.GetPersonAsync(id));
        }

        [HttpPost]
        [ValidateModel]
        [ApiAntiForgeryToken]
        [Route("add")]
        public async Task<IHttpActionResult> Add(PersonModel personModel)
        {
            await _service.AddPersonAsync(personModel);

            return Ok();
        }

        [HttpPut]
        [ValidateModel]
        [ApiAntiForgeryToken]
        [Route("update")]
        public async Task<IHttpActionResult> Update(PersonModel personModel)
        {
            await _service.UpdatePersonAsync(personModel);

            return Ok();
        }
    }
}
