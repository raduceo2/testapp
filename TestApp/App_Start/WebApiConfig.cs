﻿
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Elmah.Contrib.WebApi;
using Newtonsoft.Json.Serialization;
using TestApp.Filters;

namespace TestApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Add camel casing for objects returned by Web Api to adhere to JavaScript standard
            var jsonFormater = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormater.SerializerSettings.ContractResolver =
                new CamelCasePropertyNamesContractResolver();

            //xml formatter removed as this cause a clash when mapping Person object to PersonModel - (KnowType) serialization
            jsonFormater.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Elmah loggin for Web Api
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());
            //Exception handler
            config.Services.Replace(typeof(IExceptionHandler), new ApiExceptionHandler());
            //Model validation
            //config.Filters.Add(new ValidateModelAttribute());
        }
    }
}
