﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestApp.Filters
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            var type = exception.GetType();
            var message = exception.Message;

            //Dont show for localhost
            if (filterContext.HttpContext.Request.Url != null
                && filterContext.HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) != "http://localhost:58828")
            {
                if (exception.GetType() == typeof(HttpException))
                {
                    //ignores not found thumbnails
                }
                else if (exception.GetType() == typeof(InvalidOperationException))
                {
                    //when view is not found
                }
                else if (type == typeof(HttpAntiForgeryException))
                {
                }

                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);

                var url = new UrlHelper(filterContext.RequestContext);
                var loginUrl = url.Content("~/error/apperror");
                filterContext.HttpContext.Response.Redirect(loginUrl);
            }
        }
    }
}