﻿(function() {
    "use strict";

    angular.module("testApp")
        .controller("PersonCtrl",
            [ "growl", "PersonFactory", personCtrl]);

    function personCtrl(growl, PersonFactory) {
        //public members
        var vm = this;
        vm.allPersons = [];
        vm.currentPerson = {};

        vm.addPerson = addPerson;
        vm.closeModal = closeModal;
        vm.enableAddPerson = enableAddPerson;
        vm.getPerson = getPerson;
        vm.updatePerson = updatePerson;

        //page load
        getAllPersons();


        function getAllPersons() {
            PersonFactory.getAllPersons()
                .then(function(data) {
                    vm.allPersons = data;
                    },
                    function(error) {
                        growl.error(error.data.message, { title: "Error loading persons" });
                    });
        }

        function getPerson(id) {
            vm.update = true;
            PersonFactory.getPerson(id)
                    .then(function (data) {
                        vm.currentPerson = data;
                        $("#personModal").modal("show");
                    },
                    function (error) {
                        growl.error(error.data.message, { title: "Error loading person" });
                    });
        }

        function enableAddPerson() {
            vm.update = false;
            $("#personModal").modal("show");
        }

        function addPerson(personModel) {
                PersonFactory.addPerson(personModel)
                    .then(function () {
                        growl.success("Person added", { title: "Success" });
                    },
                     function (error) {
                         growl.error(error.data.message, { title: "Error adding person" });
                     });

        }

        function updatePerson(personModel) {
            PersonFactory.updatePerson(personModel)
                    .then(function () {
                        growl.success("Person updated", { title: "Success" });
                    },
                     function (error) {
                         growl.error(error.data.message, { title: "Error updating person" });
                     });
        }

        function closeModal() {
            location.reload();
        }


    }
})();