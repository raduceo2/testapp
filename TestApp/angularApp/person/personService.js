﻿(function () {
    "use strict";

    angular.module("testApp")
        .factory("PersonFactory", ["$http", "$q", personFactory]);

    function personFactory($http, $q) {
        return {
            addPerson: addPerson,
            getAllPersons: getAllPersons,
            getPerson: getPerson,
            updatePerson: updatePerson
     }

        function getPerson(id) {
            var deffered = $q.defer();
            $http.get("../api/person/get/" + id)
                .then(function (result) {
                    deffered.resolve(result.data);
                }, deffered.reject);

            return deffered.promise;
        }

        function getAllPersons() {
            var deffered = $q.defer();
            $http.get("../api/person/all")
                .then(function (result) {
                    deffered.resolve(result.data);
                }, deffered.reject);

            return deffered.promise;
        }

        function addPerson(person) {
            var deffered = $q.defer();
            $http.post("../api/person/add", person)
                .then(function (result) {
                    deffered.resolve(result.data);
                }, deffered.reject);

            return deffered.promise;
        }

        function updatePerson(person) {
            var deffered = $q.defer();
            $http.put("../api/person/update", person)
                .then(function (result) {
                    deffered.resolve(result.data);
                }, deffered.reject);

            return deffered.promise;
        }
    }

})();