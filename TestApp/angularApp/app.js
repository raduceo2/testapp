﻿(function () {

    angular.module("testApp", [
            "ui.bootstrap"
            , "angular-growl"
            , "ngAnimate"
    ])
    .config(["growlProvider", function (growlProvider) {
        growlProvider.globalTimeToLive(10000);
    }])
    .run([
        "$http", function ($http) {
            "use strict";
            $http.defaults.headers.common["X-XSRF-Token"] =
                angular.element("input[name='__RequestVerificationToken']").attr("value");
        }
    ]);

})();