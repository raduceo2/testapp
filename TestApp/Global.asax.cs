﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace TestApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    var exception = Server.GetLastError();
        //    Server.ClearError();
        //    var type = exception.GetType();
        //    var message = exception.Message;

        //    //Dont show for localhost
        //    if (HttpContext.Current != null
        //        && HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) != "http://localhost:51082")
        //    {
        //        if (exception.GetType() == typeof(HttpException))
        //        {
        //            //ignores not found thumbnails
        //        }
        //        else if (exception.GetType() == typeof(InvalidOperationException))
        //        {
        //            //when view is not found
        //        }
        //        else if (type == typeof(HttpAntiForgeryException))
        //        {
        //        }
        //        //...

        //        Elmah.ErrorSignal.FromCurrentContext().Raise(exception);

        //        Response.Redirect("~/Error/AppError");
        //    }
        //}
    }
}
