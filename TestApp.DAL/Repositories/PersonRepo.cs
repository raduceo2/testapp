﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TestApp.DAL.DataModels;
using System.Data.SqlClient;

namespace TestApp.DAL.Repositories
{
    public interface IRepo<T> where T : IPerson
    {
        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();
        Task<T> Get(int id);
        Task Add(T person);
        Task Update(T person);
    }

    public class PersonRepo : IRepo<IPerson>
    {
        private readonly string _connectionString;
        private const string _selectAllCommand = "select * from person";
        private const string _getPerson = "select * from person where PersonId = @id";
        private const string _insertPerson =
            "insert into person values(@firstname, @surname, @gender, @emailAddress, @phoneNo)";

        private const string _updatePerson =
            @"update table person firstnamee = @firstname,
                surname = @surname,
                gender = @gender,
                emailAddress = @emaiAddress,
                phoneNumber = @phoneNo,
                where personId = @id ";

        public PersonRepo(string connectionString)
        {
            _connectionString = connectionString;
        }

        public PersonRepo()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString; ;
        }

        public async Task<IEnumerable<IPerson>> GetAllAsync()
        {
            var listOfPersons = new List<Person>();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_selectAllCommand, connection))
                {
                    connection.Open();
                    using (var reader = await command.ExecuteReaderAsync())
                    {

                        while (reader.Read())
                        {
                            var person = new Person
                            {
                                PersonId = int.Parse(reader[nameof(Person.PersonId)].ToString()),
                                Firstname = reader[nameof(Person.Firstname)].ToString(),
                                Surname = reader[nameof(Person.Surname)].ToString(),
                                Gender = reader[nameof(Person.Gender)].ToString(),
                                EmailAddress = reader[nameof(Person.EmailAddress)].ToString(),
                                PhoneNumber = reader[nameof(Person.PhoneNumber)] as string,
                            };

                            listOfPersons.Add(person);
                        }
                    }
                }
            }

            return listOfPersons;
        }

        public IEnumerable<IPerson> GetAll()
        {
            var listOfPersons = new List<Person>();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_selectAllCommand, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var person = new Person
                            {
                                Firstname = reader[nameof(Person.Firstname)].ToString(),
                                Surname = reader[nameof(Person.Surname)].ToString(),
                                Gender = reader[nameof(Person.Gender)].ToString(),
                                EmailAddress = reader[nameof(Person.EmailAddress)].ToString(),
                                PhoneNumber = reader[nameof(Person.PhoneNumber)] as string,
                            };

                            listOfPersons.Add(person);
                        }
                    }
                }
            }

            return listOfPersons;
        }

        public async Task<IPerson> Get(int id)
        {
            var person = new Person();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_getPerson, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    connection.Open();
                    using (var reader = await command.ExecuteReaderAsync())
                    {

                        while (reader.Read())
                        {
                            person.Firstname = reader[nameof(Person.Firstname)].ToString();
                            person.Surname = reader[nameof(Person.Surname)].ToString();
                            person.Gender = reader[nameof(Person.Gender)].ToString();
                            person.EmailAddress = reader[nameof(Person.EmailAddress)].ToString();
                            person.PhoneNumber = reader[nameof(Person.PhoneNumber)] as string;

                        }
                    }
                }
            }

            return  person;
        }

        public async Task Add(IPerson person)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_insertPerson, connection))
                {
                    command.Parameters.AddWithValue("@firstname", person.Firstname);
                    command.Parameters.AddWithValue("@surname", person.Surname);
                    command.Parameters.AddWithValue("@gender", person.Gender);
                    command.Parameters.AddWithValue("@emailAddress", person.EmailAddress);
                    command.Parameters.AddWithValue("@phoneNo", person.PhoneNumber);

                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task Update(IPerson person)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_updatePerson, connection))
                {
                    command.Parameters.AddWithValue("@firstname", person.Firstname);
                    command.Parameters.AddWithValue("@surname", person.Surname);
                    command.Parameters.AddWithValue("@gender", person.Gender);
                    command.Parameters.AddWithValue("@emailAddress", person.EmailAddress);
                    command.Parameters.AddWithValue("@phoneNo", person.PhoneNumber);

                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        #region MyRegion
        private async Task AddOrUpdate(IPerson person, string query)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", person.PersonId);
                    command.Parameters.AddWithValue("@firstname", person.Firstname);
                    command.Parameters.AddWithValue("@surname", person.Surname);
                    command.Parameters.AddWithValue("@gender", person.Gender);
                    command.Parameters.AddWithValue("@emailAddress", person.EmailAddress);
                    command.Parameters.AddWithValue("@phoneNo", person.PhoneNumber);

                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }
        }


        #endregion
    }
}
