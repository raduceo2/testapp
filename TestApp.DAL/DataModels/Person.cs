﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.DAL.DataModels
{
    public interface IPerson
    {
        int PersonId { get; set; }
        string Firstname { get; set; }
        string Surname { get; set; }
        string Gender { get; set; }
        string EmailAddress { get; set; }
        string PhoneNumber { get; set; }
    }

    public class Person : IPerson
    {
        public int PersonId { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}
