﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TestApp.DAL.DataModels;
using System.Data.SqlClient;

namespace TestApp.DAL.Repositories
{
    public class PersonRepo
    {
        protected readonly string _connectionString;
        private const string _selectAllCommand = "select * from person";
            
        public PersonRepo(string connectionString)
        {
            _connectionString = connectionString;
        }  

        public IEnumerable<IPerson> GetAll()
        {
            var listOfPersons = new List<Person>();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(_selectAllCommand, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while(reader.Read())
                    {
                        var person = new Person
                        {
                            Firstname = reader["Firstname"].ToString()
                        };

                        listOfPersons.Add(person);
                    }
                }
            }

            return listOfPersons;
        }
    }
}
